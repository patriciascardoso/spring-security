package br.com.aula.itau.seguranca.repositories;

import br.com.aula.itau.seguranca.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository <Usuario, Long> {

    Usuario findByEmail(String email);
}
